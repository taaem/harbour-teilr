TARGET = harbour-teilr

CONFIG += sailfishapp kqoauth

SOURCES += src/harbour-teilr.cpp \
    src/tumblrauth.cpp \
    src/tumblrservice.cpp \
    src/helper.cpp

DISTFILES += qml/harbour-teilr.qml \
    qml/cover/CoverPage.qml \
    rpm/harbour-teilr.spec \
    rpm/harbour-teilr.yaml \
    translations/*.ts \
    rpm/harbour-teilr.changes \
    harbour-teilr.desktop \
    qml/pages/AuthPage.qml \
    qml/pages/DashboardPage.qml \
    qml/pages/StartupPage.qml \
    qml/delegates/PostDelegate.qml \
    qml/pages/ImagePage.qml \
    qml/delegates/PostHeader.qml \
    qml/components/Image.qml \
    qml/delegates/PostFooter.qml \
    qml/pages/BlogPage.qml \
    qml/pages/CommentPage.qml \
    qml/pages/SearchPage.qml \
    qml/pages/AboutPage.qml \
    harbour-teilr.png \
    qml/images/harbour-teilr.png \
    qml/pages/MyBlogPage.qml \
    rpm/harbour-teilr.changes \
    qml/components/Footer.qml \
    qml/components/VideoPlayer.qml \
    qml/pages/VideoPage.qml \
    qml/delegates/types/Answer.qml \
    qml/delegates/types/Link.qml \
    qml/delegates/types/Photo.qml \
    qml/delegates/types/Quote.qml \
    qml/delegates/types/Text.qml \
    qml/delegates/types/Video.qml

SAILFISHAPP_ICONS = 86x86 108x108 128x128 172x172

CONFIG += sailfishapp_i18n

TRANSLATIONS += translations/harbour-teilr-de.ts

HEADERS += \
    src/tumblrauth.h \
    src/tumblrservice.h \
    src/privatekeys.h \
    src/helper.h

DEPLOYMENT_PATH = /usr/share/$${TARGET}
lib.files += \
    /usr/lib/libkqoauth.so \
    /usr/lib/libkqoauth.so.0 \
    /usr/lib/libkqoauth.so.0.97 \
    /usr/lib/libkqoauth.so.0.97.0
lib.path = $$DEPLOYMENT_PATH/lib
INSTALLS += lib
