import QtQuick 2.0
import QtMultimedia 5.6
import Sailfish.Silica 1.0

Item {
    property string source
    anchors.fill: parent
    anchors.horizontalCenter: parent.horizontalCenter

    ViewPlaceholder {
        id: errorComponent

        enabled: false
        text: qsTr("Error")
        hintText: qsTr("There was an error playing this video.")
        Component.onCompleted: anchors.fill = parent
    }

    Video {
        id: video
        width: parent.width
        anchors.fill: parent
        autoLoad: true
        autoPlay: true
        anchors.horizontalCenter: parent.horizontalCenter
        source: parent.source
        onPositionChanged: videoSlider.value = position
        onErrorChanged: errorComponent.enabled
        // Waits for Qt 5.9 update
        // loops: MediaPlayer.Infinite
        onStatusChanged: {
            switch (status) {
            case MediaPlayer.NoMedia:
                console.log("NoMedia")
                videoBusyIndicator.visible = false
                break
            case MediaPlayer.Loading:
                console.log("Loading")
                videoBusyIndicator.visible = true
                break
            case MediaPlayer.Loaded:
                console.log("Loaded")
                videoBusyIndicator.visible = false
                break
            case MediaPlayer.Buffering:
                console.log("Buffering")
                videoBusyIndicator.visible = true
                break
            case MediaPlayer.Buffered:
                console.log("Buffered")
                videoBusyIndicator.visible = false
                break
            case MediaPlayer.Stalled:
                console.log("Stalled")
                videoBusyIndicator.visible = true
                break
            case MediaPlayer.EndOfMedia:
                console.log("EndOfMedia")
                controlsItem.visible = true
                videoBusyIndicator.visible = false
                break
            case MediaPlayer.InvalidMedia:
                console.log("InvalidMedia")
                videoBusyIndicator.visible = false
                errorComponent.enabled = true
                break
            case MediaPlayer.UnknownStatus:
                console.log("UnknownStatus")
                videoBusyIndicator.visible = false
                break
            }
        }

        MouseArea {
            width: parent.width
            height: parent.height - (controlsItem.visible ? controlsItem.height : 0)
            onClicked: {
                controlsItem.visible = !controlsItem.visible
            }
        }
    }

    BusyIndicator {
        id: videoBusyIndicator
        anchors.centerIn: parent
        visible: false
        running: visible
        size: BusyIndicatorSize.Medium
    }

    Timer {
        id: controlsTimer
        repeat: false
        interval: 2000
        onTriggered: {
            if (videoSlider.down) {
                restart()
            } else {
                controlsItem.visible = false
            }
        }
    }

    Item {
        id: controlsItem
        visible: false
        width: parent.width
        height: childrenRect.height
        anchors.left: parent.left
        anchors.bottom: parent.bottom
        anchors.bottomMargin: Theme.paddingMedium
        opacity: visible ? 1 : 0
        Behavior on opacity {
            NumberAnimation {
            }
        }

        Column {
            width: parent.width
            Slider {
                id: videoSlider
                width: parent.width
                anchors.horizontalCenter: parent.horizontalCenter
                minimumValue: 0
                maximumValue: (video.seekable) ? video.duration : 1
                stepSize: 1
                enabled: video.seekable
                valueText: {
                    var seconds = Math.round(value / 1000)
                    var minutes = Math.floor(seconds / 60)
                    seconds -= minutes * 60
                    if (seconds < 10) {
                        seconds = "0" + seconds
                    }
                    return minutes + ":" + seconds
                }

                onReleased: {
                    video.seek(Math.floor(value))
                    video.play()
                    controlsTimer.restart()
                }
                onValueChanged: {
                    controlsTimer.restart()
                }
            }

            IconButton {
                width: Theme.iconSizeMedium
                height: Theme.iconSizeMedium
                anchors.horizontalCenter: parent.horizontalCenter

                icon.source: ((video.playbackState == MediaPlayer.PausedState || video.status == MediaPlayer.EndOfMedia) ? "image://theme/icon-m-play?" : "image://theme/icon-m-pause?")
                             + (pressed ? Theme.highlightColor : Theme.primaryColor)
                onClicked: {
                    if (video.playbackState == MediaPlayer.PlayingState) {
                        video.pause()
                    } else {
                        video.play()
                    }
                    controlsTimer.restart()
                }
            }
        }
    }
}
