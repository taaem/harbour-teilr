/*****************************************************************************
 * Image.qml
 *
 * Created: 11.09.2016 2016 by taaem
 *
 * Copyright 2016 taaem. All rights reserved.
 *
 * This file may be distributed under the terms of GNU Public License version
 * 2 (GPL v2) as defined by the Free Software Foundation (FSF). A copy of the
 * license should have been included with this file, or the project in which
 * this file belongs to. You may also find the details of GPL v2 at:
 * http://www.gnu.org/licenses/gpl-2.0.txt
 *
 * If you have any questions regarding the use of this file, feel free to
 * contact the author of this file, or the owner of the project in which
 * this file belongs to.
*****************************************************************************/

import QtQuick 2.0
import Sailfish.Silica 1.0

Item{
    id: imgItem
    height: width * aspectRatio
    property string source
    property double aspectRatio
    property bool gif

    Loader {
        id: imgLoader
        anchors.fill: parent
        sourceComponent: (gif) ? gifComponent : imageComponent
    }

    BusyIndicator{
       id: loadingInd
       anchors.centerIn: parent
       running: true
   }

    Component {
        id: imageComponent
        Image {
            anchors.fill: parent
            id: img
            asynchronous: true
            onStatusChanged: if (img.status == Image.Ready) loadingInd.running = false

            fillMode: Image.PreserveAspectFit
            Component.onCompleted: source = imgItem.source
        }
    }
    Component {
        id: gifComponent
        AnimatedImage {
            anchors.fill: parent
            id: img
            asynchronous: true

            onStatusChanged: if (img.status == Image.Ready) loadingInd.running = false
            fillMode: Image.PreserveAspectFit
            Component.onCompleted: source = imgItem.source
        }
    }
}
