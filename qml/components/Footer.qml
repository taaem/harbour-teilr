import QtQuick 2.0
import Sailfish.Silica 1.0

Rectangle {
    property var loadMorePosts
    property bool loading: false
    id: loadMoreRow
    height: Theme.fontSizeMedium + (Theme.paddingLarge * 2)
    width: parent.width
    color: "transparent"

    Button {
        id: loadMoreButton
        text: qsTr("Load more posts")
        preferredWidth: Theme.buttonWidthLarge
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
        visible: !loading
        onClicked: loadMorePosts()
    }
    BusyIndicator {
        id: loadMoreBusyIndicator
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
        size: BusyIndicatorSize.Medium
        visible: loading
    }
}
