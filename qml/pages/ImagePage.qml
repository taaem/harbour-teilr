/*****************************************************************************
 * ImagePage.qml
 *
 * Created: 11.09.2016 2016 by taaem
 *
 * Copyright 2016 taaem. All rights reserved.
 *
 * This file may be distributed under the terms of GNU Public License version
 * 2 (GPL v2) as defined by the Free Software Foundation (FSF). A copy of the
 * license should have been included with this file, or the project in which
 * this file belongs to. You may also find the details of GPL v2 at:
 * http://www.gnu.org/licenses/gpl-2.0.txt
 *
 * If you have any questions regarding the use of this file, feel free to
 * contact the author of this file, or the owner of the project in which
 * this file belongs to.
*****************************************************************************/
import QtQuick 2.0
import Sailfish.Silica 1.0
import harbour.teilr.Helper 1.0

Page {
    id: imagePage
    property ListModel model
    property int index
    allowedOrientations: Orientation.All

    SilicaFlickable {
        anchors.fill: parent
        PullDownMenu {
            MenuItem {
                text: qsTr("Save")
                onClicked: {
                    Helper.save(Helper.Image,
                                model.get(imagePage.index).alt_sizes.get(0).url)
                }
            }
        }

        Loader {
            id: imageLoader
            anchors.centerIn: parent
            height: childrenRect.height
            width: parent.width
            anchors.margins: Theme.paddingSmall
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    if (thumbnailListView.opacity === 1) {
                        thumbnailListView.opacity = 0
                    } else {
                        thumbnailListView.opacity = 1
                    }
                }
            }
        }

        ListView {
            id: thumbnailListView
            anchors.left: parent.left
            anchors.bottom: parent.bottom
            anchors.right: parent.right
            height: visible ? Theme.itemSizeMedium : 0
            visible: count > 1
            orientation: Qt.Horizontal
            model: imagePage.model
            Behavior on opacity {
                FadeAnimation {
                }
            }

            delegate: Item {
                height: thumbnailListView.height
                width: height
                Image {
                    id: thumbnail
                    source: parent.getClosestsSize()
                    anchors.fill: parent
                    asynchronous: true
                    cache: true
                    fillMode: Image.PreserveAspectCrop
                }

                BusyIndicator {
                    anchors.fill: parent
                    anchors.centerIn: parent
                    running: thumbnail.status === Image.Loading
                }

                Rectangle {
                    anchors.fill: parent
                    color: "transparent"
                    border.color: index == imagePage.index ? Theme.highlightColor : "transparent"
                    border.width: 5
                }
                MouseArea {
                    anchors.fill: parent
                    onClicked: imagePage.index = index
                }

                function getClosestsSize() {
                    for (var i = alt_sizes.count - 1; i >= 0; i--) {
                        if (alt_sizes.get(i).height >= height) {
                            return alt_sizes.get(i).url
                        }
                    }
                }
            }
        }
    }

    onIndexChanged: setSource()

    Component.onCompleted: setSource()

    function setSource() {
        var options = {
            source: model.get(index).alt_sizes.get(0).url,
            aspectRatio: (model.get(index).alt_sizes.get(0).height / model.get(
                              index).alt_sizes.get(0).width)
        }

        if (model.get(index).alt_sizes.get(0).url.indexOf("gif") > -1) {
            options.gif = true
        }

        imageLoader.setSource("../components/Image.qml", options)
    }
}
