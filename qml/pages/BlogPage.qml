/*****************************************************************************
 * BlogPage.qml
 *
 * Created: 11.09.2016 2016 by taaem
 *
 * Copyright 2016 taaem. All rights reserved.
 *
 * This file may be distributed under the terms of GNU Public License version
 * 2 (GPL v2) as defined by the Free Software Foundation (FSF). A copy of the
 * license should have been included with this file, or the project in which
 * this file belongs to. You may also find the details of GPL v2 at:
 * http://www.gnu.org/licenses/gpl-2.0.txt
 *
 * If you have any questions regarding the use of this file, feel free to
 * contact the author of this file, or the owner of the project in which
 * this file belongs to.
*****************************************************************************/

import QtQuick 2.0
import Sailfish.Silica 1.0
import harbour.teilr.Service 1.0
import "../delegates"
import "../components" as CustomComponents

Page {
    id: page
    property bool sinBlog: true
    property string blogUrl
    property bool busy
    property bool own
    property bool followed: false
    property int index: 0
    SilicaListView {
        PullDownMenu {
            MenuItem {
                text: qsTr("Reload")
                onClicked: {
                    blog.getBlogPosts(20, index,blogUrl , [], 0, true, true)
                    viewModel.clear()
                    page.busy = true
                }
            }
            MenuItem{
                text: (page.followed)? qsTr("Unfollow") : qsTr("Follow")
                visible: !page.own
                onClicked: {
                    if(page.own !== true){
                        if(page.followed){
                            blog.followBlog(false,blogUrl)
                            page.followed = false
                        }else{
                            blog.followBlog(true,blogUrl)
                            page.followed = true
                        }
                    }
                }
            }
        }

        id: blogListView
        model: ListModel{
            id: viewModel
        }
        spacing: Theme.paddingMedium
        anchors.fill: parent
        header:PageHeader{
            id: pageHeader
            property int defaultHeight: Theme.fontSizeLarge + 128 + Theme.fontSizeSmall * 2 + 3 * Theme.paddingLarge + 3 * Theme.paddingMedium
            width: parent.width
            height: defaultHeight

            Column{
                id: headerColumn
                width: parent.width
                spacing: Theme.paddingLarge
                Label {
                    text: blog.name
                    horizontalAlignment: Text.AlignHCenter
                    wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                    font.pixelSize: Theme.fontSizeSmall
                    width: parent.width - Theme.paddingSmall
                    color: Theme.highlightColor
                }

                Label{
                    text: blog.title
                    horizontalAlignment: Text.AlignHCenter
                    wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                    font.pixelSize: Theme.fontSizeLarge
                    width: parent.width - Theme.paddingSmall
                    color: Theme.highlightColor
                }
                Image {
                    height: 128
                    id: blogAvatar
                    anchors.horizontalCenter: parent.horizontalCenter
                    source: blog.avatar
                    sourceSize.width: parent.width / 2
                }
                Rectangle {
                    id: descriptionContainer
                    property bool expanded: false
                    property int defaultHeight: Theme.fontSizeSmall + Theme.paddingMedium
                    color: "transparent"
                    width: parent.width
                    height: defaultHeight

                    OpacityRampEffect {
                        sourceItem: blogDescription
                        offset: (parent.expanded) ? 1 : 0.5
                        direction: OpacityRamp.TopToBottom
                    }

                    Label{
                        id: blogDescription
                        anchors.left: parent.left
                        anchors.leftMargin: Theme.paddingMedium
                        text: blog.description
                        wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                        onLinkActivated: Qt.openUrlExternally(url)
                        linkColor: Theme.secondaryHighlightColor
                        textFormat: Text.RichText
                        font.pixelSize: Theme.fontSizeSmall
                        width: parent.width - Theme.paddingSmall
                        color: Theme.secondaryColor
                        height: (parent.expanded) ? undefined : parent.height
                    }
                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            parent.expanded = !parent.expanded
                            if (parent.expanded) {
                                descriptionContainer.height = blogDescription.height
                                pageHeader.height += blogDescription.height
                            } else {
                                descriptionContainer.height = descriptionContainer.defaultHeight
                                pageHeader.height = pageHeader.defaultHeight
                            }
                        }
                    }
                }
            }
        }

        ViewPlaceholder {
            anchors.fill: parent
            anchors.topMargin: Theme.paddingLarge

            enabled: !blog.exists
            text: blog.errorTitle
            hintText: blog.errorSummary
        }

        footer: CustomComponents.Footer{
            id: footer
            visible: !page.busy && blog.exists && (viewModel.count % 20 === 0)
            width: parent.width
            loadMorePosts: function() {
                index = index + 20
                blog.getBlogPosts(20, index, blogUrl, [], 0, true, true)
                loading = true
            }
        }

        delegate: PostDelegate{}
        VerticalScrollDecorator {}
        BusyIndicator{
            running: page.busy && blog.exists
            anchors.centerIn: parent
        }
    }

    Tumblr{
        id: blog
        property string title: " "
        property string name: " "
        property string url: " "
        property string description: " "
        property string avatar
        property bool ask
        property bool exists: true
        property string errorTitle
        property string errorSummary
        authenticationProvider: app.authenticationProvider

        onAuthenticationProviderChanged: {
            blog.getBlog(20, page.index, blogUrl, [], 0, true, true)
            page.busy = true
        }
        onGotBlogPosts: {
            append(blog)
            page.busy = false
            blogListView.footerItem.loading = false
        }
        onGotBlogInfo:{
            title = info.title
            name = info.name
            url = info.url
            description = info.description
            ask = info.ask
            page.followed = info.followed
        }
        onGotBlogAvatar: {
            avatar = avatarUrl
        }
        onBlogNotFound: {
            console.log("Blog does not exits")
            exists = false
            errorTitle = errors[0].title
            errorSummary = errors[0].detail
        }
    }
    function append(element){
        blogListView.model.append(element)
    }
}

