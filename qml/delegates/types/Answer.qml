/*****************************************************************************
 * AnswerDelegate.qml
 *
 * Created: 11.09.2016 2016 by taaem
 *
 * Copyright 2016 taaem. All rights reserved.
 *
 * This file may be distributed under the terms of GNU Public License version
 * 2 (GPL v2) as defined by the Free Software Foundation (FSF). A copy of the
 * license should have been included with this file, or the project in which
 * this file belongs to. You may also find the details of GPL v2 at:
 * http://www.gnu.org/licenses/gpl-2.0.txt
 *
 * If you have any questions regarding the use of this file, feel free to
 * contact the author of this file, or the owner of the project in which
 * this file belongs to.
*****************************************************************************/

import QtQuick 2.0
import Sailfish.Silica 1.0

Item {
    height: childrenRect.height
    width: parent.width
    Rectangle{
        id: askingRectangle
        height: askingColumn.height
        width: parent.width
        color: Theme.rgba(Theme.highlightBackgroundColor, Theme.highlightBackgroundOpacity)
        opacity: 2
        anchors.horizontalCenter: parent.horizontalCenter
        Column{
            id: askingColumn
            width: parent.width
            Label{
                id: questionOrig
                text: asking_name
                //anchors.left: parent.left
                //anchors.leftMargin: Theme.paddingSmall
                font.pixelSize: Theme.fontSizeSmall
                color: listItem.highlighted ? Theme.highlightColor : Theme.secondaryColor
                width: parent.width
            }

            Label{
                width: parent.width - Theme.paddingMedium
                anchors.horizontalCenter: parent.horizontalCenter
                id: questionItem
                text: app.priLinkStyle + question
                onLinkActivated: Qt.openUrlExternally(url)
                textFormat: Text.RichText
                font.pixelSize: Theme.fontSizeMedium
                wrapMode: Text.Wrap
                color: listItem.highlighted ? Theme.highlightColor : Theme.primaryColor
            }
        }
    }
    Label {
        width: parent.width - Theme.paddingMedium
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: askingRectangle.bottom
        id: bodyItem
        text: app.priLinkStyle + viewModel.get(index).answer
        wrapMode: Text.Wrap
        textFormat: Text.RichText
        onLinkActivated: Qt.openUrlExternally(url)
        linkColor: Theme.highlightColor
        font.pixelSize: Theme.fontSizeSmall
        color: listItem.highlighted ? Theme.highlightColor : Theme.primaryColor
    }
}
