/*****************************************************************************
 * VideoDelegate.qml
 *
 * Created: 11.09.2016 2016 by taaem
 *
 * Copyright 2016 taaem. All rights reserved.
 *
 * This file may be distributed under the terms of GNU Public License version
 * 2 (GPL v2) as defined by the Free Software Foundation (FSF). A copy of the
 * license should have been included with this file, or the project in which
 * this file belongs to. You may also find the details of GPL v2 at:
 * http://www.gnu.org/licenses/gpl-2.0.txt
 *
 * If you have any questions regarding the use of this file, feel free to
 * contact the author of this file, or the owner of the project in which
 * this file belongs to.
*****************************************************************************/
// TODO: Fix this
import QtQuick 2.0
import Sailfish.Silica 1.0

Item {
    height: childrenRect.height
    width: parent.width
    Image {
        id: previewImage
        source : getSourcePreview()
        width: parent.width
        height: width * 9 / 16

        fillMode: Image.PreserveAspectCrop

        IconButton {
            anchors.centerIn: parent
            icon.source: "image://theme/icon-l-play?" + (pressed
                         ? Theme.highlightColor
                         : Theme.primaryColor)
            onClicked: {
                pageStack.push(Qt.resolvedUrl("../../pages/VideoPage.qml"),
                                             {source: getSource()})
            }
        }
    }
    function getSource() {
        var src = JSON.stringify(model.player.get(0)) + ""
        var srcSplit = src.split("\\\"")
        var url;
        for (var st in srcSplit) {
            if(srcSplit[st].indexOf("video_file") !== -1) {
                url = srcSplit[st]
            }
        }
        var urlSplit = url.split("/")
        url = "https://vtt.tumblr.com/" + urlSplit[6] + "_480.mp4"
        return url
    }
    function getSourcePreview() {
        var src = JSON.stringify(model.player.get(0)) + ""
        var srcSplit = src.split("'")
        var url;
        for (var st in srcSplit) {
            if(srcSplit[st].indexOf("tumblr") !== -1
                    && srcSplit[st].indexOf("previews") === -1
                    && srcSplit[st].indexOf("video_file") === -1) {
                url = srcSplit[st]
            }
        }
        return url
    }
}
