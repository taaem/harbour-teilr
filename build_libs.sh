#!/bin/sh
SAILFISH_RELEASE=$@
pushd ./kQOAuth/
sb2 -t SailfishOS-$SAILFISH_RELEASE-i486 -m sdk-install -R zypper in openssl-devel openssl
sb2 -t SailfishOS-$SAILFISH_RELEASE-armv7hl -m sdk-install -R zypper in openssl-devel openssl

sb2 -t SailfishOS-$SAILFISH_RELEASE-i486 -m sdk-build qmake CONFIG+=release
sb2 -t SailfishOS-$SAILFISH_RELEASE-i486 -m sdk-build make clean
sb2 -t SailfishOS-$SAILFISH_RELEASE-i486 -m sdk-build make
sb2 -t SailfishOS-$SAILFISH_RELEASE-i486 -m sdk-install -R make install

sb2 -t SailfishOS-$SAILFISH_RELEASE-armv7hl -m sdk-build qmake CONFIG+=release
sb2 -t SailfishOS-$SAILFISH_RELEASE-armv7hl -m sdk-build make clean
sb2 -t SailfishOS-$SAILFISH_RELEASE-armv7hl -m sdk-build make
sb2 -t SailfishOS-$SAILFISH_RELEASE-armv7hl -m sdk-install -R make install
popd
