/*****************************************************************************
 * helper.h
 *
 * Created: 11.09.2016 2016 by taaem
 *
 * Copyright 2016 taaem. All rights reserved.
 *
 * This file may be distributed under the terms of GNU Public License version
 * 2 (GPL v2) as defined by the Free Software Foundation (FSF). A copy of the
 * license should have been included with this file, or the project in which
 * this file belongs to. You may also find the details of GPL v2 at:
 * http://www.gnu.org/licenses/gpl-2.0.txt
 *
 * If you have any questions regarding the use of this file, feel free to
 * contact the author of this file, or the owner of the project in which
 * this file belongs to.
 *****************************************************************************/

#ifndef HELPER_H
#define HELPER_H

#include <QDir>
#include <QFile>
#include <QJSEngine>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QObject>
#include <QQmlEngine>
#include <QStandardPaths>

#include <sailfishapp.h>

class Helper : public QObject {
    Q_OBJECT
  public:
    explicit Helper(QObject *parent = 0);
    enum DownloadType { Image, Video };
    Q_ENUM(DownloadType)
    Q_INVOKABLE void save(DownloadType, QString);
    Q_INVOKABLE static QString getBlogFromLink(QString);
  signals:

  private slots:
    void onImageDownloadFinished(QNetworkReply *);
    void onVideoDownloadFinished(QNetworkReply *);

  private:
    QNetworkAccessManager *manager;
};

static QObject *helperProvider(QQmlEngine *engine, QJSEngine *scriptEngine) {
    Q_UNUSED(engine)
    Q_UNUSED(scriptEngine)

    Helper *helper = new Helper();
    return helper;
}

#endif // HELPER_H
