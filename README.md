## Teilr
This is an unofficial Tumblr Client for Sailfish OS.
Its an early project and can NOT considered stable.
Also many functionallity is missing.

### Building
```
git clone https://github.com/taaem/harbour-teilr.git
cd harbour-teilr

// SSH into the build VM
ssh -p 2222 -i ~/SailfishOS/vmshare/ssh/private_keys/engine/mersdk mersdk@localhost

// Go to the project directory in /home/nemo/share/<project-path>
cd /home/nemo/share/<project-path> 

// Build KQOauth
./build_libs.sh <Sailfish-Version>
```

Next you have to obtain API Keys from tumblr and put them into a file called privatekeys.h in the src/ directory.
For reference look at the examplekeys.h file.

Now you can build Teilr from the Sailfish OS IDE

### License
GPL-v2
