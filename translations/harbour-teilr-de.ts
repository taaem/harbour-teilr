<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de">
<context>
    <name>AboutPage</name>
    <message>
        <source>About</source>
        <translation>Über</translation>
    </message>
    <message>
        <source>Logout</source>
        <translation>Abmelden</translation>
    </message>
</context>
<context>
    <name>AuthPage</name>
    <message>
        <source>Loading...</source>
        <translation>Laden...</translation>
    </message>
    <message>
        <source>Reload</source>
        <translation>Neu laden</translation>
    </message>
</context>
<context>
    <name>BlogPage</name>
    <message>
        <source>Reload</source>
        <translation>Neu laden</translation>
    </message>
    <message>
        <source>Unfollow</source>
        <translation>Entfolgen</translation>
    </message>
    <message>
        <source>Follow</source>
        <translation>Folgen</translation>
    </message>
</context>
<context>
    <name>CommentPage</name>
    <message>
        <source>Add your Comment!</source>
        <translation>Füge deinen Kommentar hinzu!</translation>
    </message>
    <message>
        <source>Blog to post on</source>
        <translation>Blog auf dem der Beitrag erscheinen soll</translation>
    </message>
</context>
<context>
    <name>CoverPage</name>
    <message>
        <source>Teilr</source>
        <translation>Teilr</translation>
    </message>
</context>
<context>
    <name>DashboardPage</name>
    <message>
        <source>Your Tumblr Dashboard</source>
        <translation>Deine Tumblr Startseite</translation>
    </message>
    <message>
        <source>Search</source>
        <translation>Suche</translation>
    </message>
    <message>
        <source>About</source>
        <translation>Über</translation>
    </message>
    <message>
        <source>My Blogs</source>
        <translation>Meine Blogs</translation>
    </message>
    <message>
        <source>Reload</source>
        <translation>Neu laden</translation>
    </message>
</context>
<context>
    <name>Footer</name>
    <message>
        <source>Load more posts</source>
        <translation>Mehr Beiträge laden</translation>
    </message>
</context>
<context>
    <name>ImagePage</name>
    <message>
        <source>Save</source>
        <translation>Speichern</translation>
    </message>
</context>
<context>
    <name>MyBlogPage</name>
    <message>
        <source>My Blogs</source>
        <translation>Meine Blogs</translation>
    </message>
</context>
<context>
    <name>PostDelegate</name>
    <message>
        <source>Remove</source>
        <translation>Löschen</translation>
    </message>
    <message>
        <source>Removing</source>
        <translation>Löschen</translation>
    </message>
</context>
<context>
    <name>SearchPage</name>
    <message>
        <source>Search</source>
        <translation>Suche</translation>
    </message>
    <message>
        <source>Go to</source>
        <translation>Gehe zu </translation>
    </message>
    <message>
        <source>No search results</source>
        <translation>Keine Suchergebnisse</translation>
    </message>
</context>
<context>
    <name>StartupPage</name>
    <message>
        <source>Loading...</source>
        <translation>Laden...</translation>
    </message>
</context>
<context>
    <name>VideoPage</name>
    <message>
        <source>Save</source>
        <translation>Speichern</translation>
    </message>
</context>
<context>
    <name>VideoPlayer</name>
    <message>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <source>There was an error playing this video.</source>
        <translation>Es gab einen Fehler beim Abspielen dieses Videos.</translation>
    </message>
</context>
</TS>
