<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en">
<context>
    <name>AboutPage</name>
    <message>
        <source>About</source>
        <translation>About</translation>
    </message>
    <message>
        <source>Logout</source>
        <translation>Logout</translation>
    </message>
</context>
<context>
    <name>AuthPage</name>
    <message>
        <source>Loading...</source>
        <translation>Loading...</translation>
    </message>
    <message>
        <source>Reload</source>
        <translation>Reload</translation>
    </message>
</context>
<context>
    <name>BlogPage</name>
    <message>
        <source>Reload</source>
        <translation>Reload</translation>
    </message>
    <message>
        <source>Unfollow</source>
        <translation>Unfollow</translation>
    </message>
    <message>
        <source>Follow</source>
        <translation>Follow</translation>
    </message>
</context>
<context>
    <name>CommentPage</name>
    <message>
        <source>Add your Comment!</source>
        <translation>Add your Comment!</translation>
    </message>
    <message>
        <source>Blog to post on</source>
        <translation>Blog to post on</translation>
    </message>
</context>
<context>
    <name>CoverPage</name>
    <message>
        <source>Teilr</source>
        <translation>Teilr</translation>
    </message>
</context>
<context>
    <name>DashboardPage</name>
    <message>
        <source>Your Tumblr Dashboard</source>
        <translation>Your Tumblr Dashboard</translation>
    </message>
    <message>
        <source>Search</source>
        <translation>Search</translation>
    </message>
    <message>
        <source>About</source>
        <translation>About</translation>
    </message>
    <message>
        <source>My Blogs</source>
        <translation>My Blogs</translation>
    </message>
    <message>
        <source>Reload</source>
        <translation>Reload</translation>
    </message>
</context>
<context>
    <name>Footer</name>
    <message>
        <source>Load more posts</source>
        <translation>Load more posts</translation>
    </message>
</context>
<context>
    <name>ImagePage</name>
    <message>
        <source>Save</source>
        <translation>Save</translation>
    </message>
</context>
<context>
    <name>MyBlogPage</name>
    <message>
        <source>My Blogs</source>
        <translation>My Blogs</translation>
    </message>
</context>
<context>
    <name>PostDelegate</name>
    <message>
        <source>Remove</source>
        <translation>Remove</translation>
    </message>
    <message>
        <source>Removing</source>
        <translation>Removing</translation>
    </message>
</context>
<context>
    <name>SearchPage</name>
    <message>
        <source>Search</source>
        <translation>Search</translation>
    </message>
    <message>
        <source>Go to</source>
        <translation>Go to</translation>
    </message>
    <message>
        <source>No search results</source>
        <translation>No search results</translation>
    </message>
</context>
<context>
    <name>StartupPage</name>
    <message>
        <source>Loading...</source>
        <translation>Loading...</translation>
    </message>
</context>
<context>
    <name>VideoPage</name>
    <message>
        <source>Save</source>
        <translation>Save</translation>
    </message>
</context>
<context>
    <name>VideoPlayer</name>
    <message>
        <source>Error</source>
        <translation>Error</translation>
    </message>
    <message>
        <source>There was an error playing this video.</source>
        <translation>There was an error playing this video.</translation>
    </message>
</context>
</TS>
